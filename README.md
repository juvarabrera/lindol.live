# Lindol.Live

https://lindol.live is a web application that lists all earthquake events in the Philippines provided by the [DOST PHIVOLCS](https://earthquake.phivolcs.dost.gov.ph/).

Read more on the [About page](https://lindol.live/).

## Stack
1. Oslo.JS - it's my personal javascript framework I developed 8 years ago. It is for sure outdated but I use this for some small projects.
2. gulp - use for building the application
3. SCSS - css framework
4. GitLab CI - for automated building and deployment to AWS.
5. Django - the api service for the app. This is not unfortunately public because I hosted the api on my personal API.

## Setting Up
1. Run `npm install` to install libraries used for building

## Building
1. `cd lindol.live`
2. `gulp build` to build the application once OR;
3. `gulp watch` to start a working process that observes changes on the `oslo/` directory and builds the application

## Contributing
Any contributions are appreciated including:
1. Features/ideas you want to see on the website
2. Issues/bugs you saw on the web application.
3. Logo of the lindol.live web app
