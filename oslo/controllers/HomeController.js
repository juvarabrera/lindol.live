Oslo.Controller["HomeController"] = {
	"construct": function(parameters) {
		
	},
	"index": function(parameters) {
		let minimumMagnitude = 0;
		if(parameters.length !== 0)
			if(parameters[0] !== "")
				minimumMagnitude = parseFloat(parameters[0]);
		return Oslo.View.render(() => {
			app.home(minimumMagnitude);
		});
	},
	"view": function(parameters) {
		if(parameters.length < 1) {
			return "/home/index";
		}
		if(parameters[0] === "") {
			return "/home/index";
		}
		return Oslo.View.render(() => {
			app.viewEvent(parameters[0]);
		});
	}
}