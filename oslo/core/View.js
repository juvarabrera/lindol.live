Oslo["View"] = {
	"construct": function(parameters) {
		this.setTemplate(Oslo.Constant.DEFAULT_TEMPLATE);
	},
	"setTemplate": function(template) {
		this["TEMPLATE"] = template;
		return this;
	},
	"data": {},
	"set": function(name, val) {
		this.data[name] = val;
	},
	"setJson": function(json) {
		for(var i in json)
			this.data[i] = json[i];
	},
	"render": function(callback) {
		if(document.querySelector("#view") != null) {
			this.loadPage(callback);
		} else {
			let template = this.TEMPLATE;
			fetch(`views/${template}.html`).then(response => {
				return response.text()
			}).then(html => {
				$(".app").html(html);
				Oslo.View.loadPage(callback);
			}).catch(error => {
				console.log(error);
				Oslo.location("error", "template-not-found", [template, Oslo.Utility.getControllerCode(Oslo.CONTROLLER), Oslo.ACTION])
			});
		}
		return true;
	},
	"loadPage": function(callback) {
		fetch(`views/${Oslo.Utility.getControllerCode(Oslo.CONTROLLER)}/${Oslo.ACTION}.html`).then(response => {
			return response.text();
		}).then(content => {
			for(var i in Oslo.View.data) {
				content = content.replace(new RegExp("{{"+i+"}}", "g"), Oslo.View.data[i]);
			}
			$("<div></div>").html(content).children().each(function() {
				$(".app").find("#"+$(this).attr("id")).html($(this).html());
			});
			$(document).scrollTop(0)
			$(".app").attr("oslo-controller", Oslo.Utility.getControllerCode(Oslo.CONTROLLER))
						.attr("oslo-action", Oslo.ACTION);
			Oslo.View.destruct();
			if (typeof callback === 'function') {
				callback();
			}
		}).catch(error => {
			console.log(error);
			
		});
	},
	"destruct": function() {
		// $(`nav a`).removeClass("active");
		// $(`nav a[href="#/${Oslo.Utility.getControllerCode(Oslo.CONTROLLER)}"]`).addClass("active");
	}
}