class LindolLive {
    constructor() {
        this.API_KEY = "AIzaSyBJX30hX6gdI0ZXYQ5QFc6sXE54zVaaUV4";
        this.MAP_ID = "55cc8013ad35c7f2";
        this.map = null;
        this.library = {};
        this.points = [];
        (g=>{var h,a,k,p="The Google Maps JavaScript API",c="google",l="importLibrary",q="__ib__",m=document,b=window;b=b[c]||(b[c]={});var d=b.maps||(b.maps={}),r=new Set,e=new URLSearchParams,u=()=>h||(h=new Promise(async(f,n)=>{await (a=m.createElement("script"));e.set("libraries",[...r]+"");for(k in g)e.set(k.replace(/[A-Z]/g,t=>"_"+t[0].toLowerCase()),g[k]);e.set("callback",c+".maps."+q);a.src=`https://maps.${c}apis.com/maps/api/js?`+e;d[q]=f;a.onerror=()=>h=n(Error(p+" could not load."));a.nonce=m.querySelector("script[nonce]")?.nonce||"";m.head.append(a)}));d[l]?console.warn(p+" only loads once. Ignoring:",g):d[l]=(f,...n)=>r.add(f)&&u().then(()=>d[l](f,...n))})({
            key: this.API_KEY,
            v: "weekly",
        });
        this.events = [];
        this.DEFAULT_COORDS = {
            "lat": 14.542069,
            "lng": 120.942069
        };
        this.loading = false;
        this.charts = {}
    }
    pause() {
        return new Promise(resolve => {
            setTimeout(() => {
                resolve()
            }, 1000);
        })
    }
    async home(minimumMagnitude) {
        const { Map } = await google.maps.importLibrary("maps");
        this.library["Map"] = Map;
        this.showLoading(true, 1);
        await this.pause()
        this.initMap("map", this.DEFAULT_COORDS, 8);
        this.renderPrevious(await this.getData(null, minimumMagnitude));
        this.showLoading(false);
        document.querySelector(".event:not([data-template])").click();
        const eventsContainer = document.querySelector(".events-container");
        eventsContainer.addEventListener("scroll", async () => {
            if(eventsContainer.scrollTop + eventsContainer.clientHeight >= eventsContainer.scrollHeight - 1 && !this.loading) {
                const lastDiv = eventsContainer.querySelector('div.event:last-child');
                const id = lastDiv.getAttribute("data-id");
                this.showLoading(true, 2);
                const moreEvents = await this.getData(id, minimumMagnitude);
                this.renderPrevious(moreEvents);
                this.showLoading(false);
            }
        })
    }
    
    async reports() {
        const { Map } = await google.maps.importLibrary("maps");
        this.library["Map"] = Map;
        this.initMap("heatMapDistribution", {
            "lat": 12.216698,
            "lng": 122.287356
        }, 6);
        const data = await this.getReport();
        this.renderCharts(data);
        this.initSlider("magnitude", {
            "values": ["0", "3", "4.5", "5.5", "6.5", "7.5", "10"],
            "unique": true
        });
        document.getElementById("start_date").value = data.report[0].month_year;
        const start_datetime = new Date(data.report[0].month_year);
        const end_datetime = new Date();
        const months = (end_datetime.getFullYear() - start_datetime.getFullYear()) * 12 + (end_datetime.getMonth() - start_datetime.getMonth());
        this.initSlider("date", {
            "minimum": 0,
            "maximum": months,
            "start_datetime": start_datetime,
            "step": 1,
            "isDate": true
        });
    }
    renderCharts(data) {
        const timeSeriesAnalysisContainer = document.getElementById("timeSeriesAnalysisChart");
        if("monthlyEarthquakeMagnitudeDistribution" in this.charts) {
            this.charts["monthlyEarthquakeMagnitudeDistribution"].destroy();
        }
        const reports = data.report;
        reports.reverse()
        this.charts["monthlyEarthquakeMagnitudeDistribution"] = new Chart(timeSeriesAnalysisContainer, {
            type: 'bar',
            responsive: false,
            maintainAspectRatio: false,
            scales: {
                x: {
                    stacked: true,
                },
                y: {
                    stacked: true
                }
            },
            data: {
                labels: reports.map(row => {
                    const date = new Date(row.month_year);
                    const options = { year: 'numeric', month: 'long' };
                    return date.toLocaleDateString('en-US', options);
                }),
                datasets: [
                    {
                        label: 'mag < 3',
                        data: reports.map(row => {
                            return row.earthquakes_lt_3_0
                        }),
                        stack: "magnitude",
                        backgroundColor: "#7efbdf"
                    }, {
                        label: '3 >= mag < 4.5',
                        data: reports.map(row => {
                            return row.earthquakes_gte_3_0_lt_4_5
                        }),
                        stack: "magnitude",
                        backgroundColor: "#95f879"
                    }, {
                        label: '4.5 >= mag < 5.5',
                        data: reports.map(row => {
                            return row.earthquakes_gte_4_5_lt_5_5
                        }),
                        stack: "magnitude",
                        backgroundColor: "#f7f835"
                    }, {
                        label: '5.5 >= mag < 6.5',
                        data: reports.map(row => {
                            return row.earthquakes_gte_5_5_lt_6_5
                        }),
                        stack: "magnitude",
                        backgroundColor: "#fdca2c"
                    }, {
                        label: '6.5 >= mag < 7.5',
                        data: reports.map(row => {
                            return row.earthquakes_gte_6_5_lt_7_5
                        }),
                        stack: "magnitude",
                        backgroundColor: "#ff701f"
                    }, {
                        label: '7.5 >= mag',
                        data: reports.map(row => {
                            return row.earthquakes_gte_7_5
                        }),
                        stack: "magnitude",
                        backgroundColor: "#ec2516"
                    }
                ]
            }
        });
        this.removePoints()
        for(const index in data.events) {
            const event = data.events[index];
            const coords = {
                "lat": parseFloat(event.latitude),
                "lng": parseFloat(event.longitude)
            };
            this.addPoint(this.map, event.magnitude, coords, true, true);
        }
    }
    async refreshReport() {
        const startDate = document.getElementById("start_date").value;
        const magnitude = this.getSliderValues("magnitude");
        const date = this.getSliderValues("date");
        const data = await this.getReport(magnitude, date, startDate);
        this.renderCharts(data);
    }
    getSliderValues(name) {
        const sliderContainer = document.querySelector(`.dual-slider[data-slider="${name}"]`);
        const slider1 = sliderContainer.querySelector(`input[name="slider1"]`);
        const slider2 = sliderContainer.querySelector(`input[name="slider2"]`);
        const slider1Value = parseFloat(slider1.value);
        const slider2Value = parseFloat(slider2.value);
        let minimum = slider1Value;
        let maximum = slider2Value;
        if(slider2Value < slider1Value) {
            maximum = slider1Value;
            minimum = slider2Value;
        }
        return [minimum, maximum]
    }
    initSlider(name, options) {
        const sliderContainer = document.querySelector(`.dual-slider[data-slider="${name}"]`);
        const valuesContainer = document.querySelector(`.dual-slider-values[data-slider="${name}"]`);
        const slider1 = sliderContainer.querySelector(`input[name="slider1"]`);
        const slider2 = sliderContainer.querySelector(`input[name="slider2"]`);
        if("minimum" in options && "maximum" in options && "step" in options) {
            slider1.min = options.minimum;
            slider2.min = options.minimum;
            slider1.max = options.maximum;
            slider2.max = options.maximum;
            slider1.step = options.step;
            slider2.step = options.step;
            slider1.value = options.minimum;
            slider2.value = options.maximum;
        } else if("values" in options) {
            slider1.min = 0;
            slider2.min = 0;
            slider1.max = options.values.length - 1;
            slider2.max = options.values.length - 1;
            slider1.step = 1;
            slider2.step = 1;
            slider1.value = 0;
            slider2.value = options.values.length - 1;
        }
        const getValues = function(e) {
            const slider1Value = parseFloat(slider1.value);
            const slider2Value = parseFloat(slider2.value);
            let minimum = slider1Value;
            let maximum = slider2Value;
            if(slider2Value < slider1Value) {
                maximum = slider1Value;
                minimum = slider2Value;
            }
            if("isDate" in options) {
                if(options["isDate"]) {
                    const date_options = { year: 'numeric', month: 'long' };
                    const start_datetime = new Date(options.start_datetime);
                    const end_datetime = new Date(options.start_datetime);
                    start_datetime.setMonth(start_datetime.getMonth() + minimum);
                    end_datetime.setMonth(end_datetime.getMonth() + maximum);
                    valuesContainer.querySelector("span.min").textContent = start_datetime.toLocaleDateString('en-US', date_options);
                    valuesContainer.querySelector("span.max").textContent = end_datetime.toLocaleDateString('en-US', date_options);
                    return;
                }
            } else if("values" in options) {
                valuesContainer.querySelector("span.min").textContent = options.values[minimum];
                valuesContainer.querySelector("span.max").textContent = options.values[maximum];
                return;
            }
            valuesContainer.querySelector("span.min").textContent = minimum;
            valuesContainer.querySelector("span.max").textContent = maximum;
        }
        getValues();
        slider1.addEventListener("input", getValues);
        slider2.addEventListener("input", getValues);
        const checkIfUnique = () => {
            if("unique" in options) {
                if(options.unique) {
                    const v1 = parseInt(slider1.value);
                    const v2 = parseInt(slider2.value);
                    if(v1 ===  v2) {
                        if(v2 === options.values.length - 1) {
                            slider1.value = v2 - 1;
                        } else {
                            slider2.value = v1 + 1;
                        }
                        getValues();
                    }
                }
            }
        }
        const changeSliderListener = () => {
            checkIfUnique();
            this.refreshReport()
        }
        slider1.addEventListener("change", () => {
            changeSliderListener();
        });
        slider2.addEventListener("change", () => {
            changeSliderListener();
        });
    }
    showLoading(show = false, type = 1) {
        this.loading = show;
        const eventsContainer = document.querySelector(".events-container");
        const loadingContainer = eventsContainer.querySelector(".loading-container");
        if(show) {
            const loadingEvents = document.querySelector(`[data-template='loading-events-${type}']`).cloneNode(true);
			loadingEvents.removeAttribute("data-template");
			eventsContainer.insertAdjacentElement("beforeend", loadingEvents);
            if(type === 2)
                eventsContainer.scrollTop = eventsContainer.scrollHeight;
        } else {
            loadingContainer.remove();
        }
    }
    initMap(id, coords, zoom) {
        this.map = new this.library.Map(document.getElementById(id), {
            center: coords,
            zoom: zoom,
            mapId: this.MAP_ID
        });
    }
    addPoint(map, magnitude, coords, include_marker = false, include_circle = false) {
        const circleColor = {
            "category-1": "#77dbb1",
            "category-2": "#52be91",
            "category-3": "#e9c53d",
            "category-4": "#F8B003",
            "category-5": "#e44d67",
            "category-6": "#c72842",
        }
        const point = {};
        if(include_marker)
            point["marker"] = new google.maps.Marker({
                map: map,
                position: coords
            });
        if(include_circle)
            point["circle"] = new google.maps.Circle({
                strokeColor: circleColor[this.getCategory(magnitude)],
                strokeOpacity: 0.5,
                strokeWeight: 2,
                fillColor: circleColor[this.getCategory(magnitude)],
                fillOpacity: 0.1,
                map: map,
                center: coords,
                radius: (Math.exp (magnitude/1.01-0.13))*1000,
            });
        this.points.push(point);
    }
    removePoints() {
        for(const index in this.points) {
            const point = this.points[index];
            if("marker" in point)
                point.marker.setMap(null);
            if("circle" in point)
                point.circle.setMap(null);
        }
        this.points = [];
    }
    getData(lastId = null, minimumMagnitude = 0) {
        return new Promise(async (resolve) => {
            const response = await fetch(`https://api.juvarabrera.com/lindollive/latest?magnitude=${minimumMagnitude}${lastId !== null ? "&lastId=" + lastId : ""}`);
            const data = await response.json();
            resolve(data.events);
        })
    }
    getReport(magnitude = [], date = [], startDate = null) {
        return new Promise(async (resolve) => {
            let url = `https://api.juvarabrera.com/lindollive/dashboard?`;
            if(magnitude.length === 2) {
                url += "&magnitude=" + magnitude.join(",");
            }
            if(date.length === 2) {
                url += "&date=" + date.join(",");
            }
            if(startDate !== null) {
                url += "&startDate=" + startDate;
            }
            const response = await fetch(url);
            const data = await response.json();
            resolve(data);
        })
    }
    getEvent(eventId) {
        return new Promise(async (resolve, reject) => {
            try {
                const response = await fetch(`https://api.juvarabrera.com/lindollive/event/${eventId}`);
                const data = await response.json();
                if(data.success)
                    resolve(data.event);
                else
                reject();
            } catch {
                reject();
            }
        })
    }
    async viewEvent(eventId) {
        const { Map } = await google.maps.importLibrary("maps");
        this.library["Map"] = Map;
        this.showLoading(true, 1);
        await this.pause()
        this.initMap("map", this.DEFAULT_COORDS, 8);
        const eventData = await this.getEvent(eventId);
        const eventsContainer = document.querySelector(".events-container");
        const eventTemplate = document.querySelector("[data-template='event']");
        const eventContainer = eventTemplate.cloneNode(true);
        eventContainer.setAttribute("data-latitude", eventData.latitude);
        eventContainer.setAttribute("data-longitude", eventData.longitude);
        eventContainer.setAttribute("data-magnitude", eventData.magnitude);
        eventContainer.removeAttribute("data-template");
        eventsContainer.insertAdjacentElement("beforeend", eventContainer);
        this.renderEvent(eventContainer, eventData);
        this.showLoading(false);
        this.openEvent(eventContainer);
    }
    renderEvent(container, event) {
        const magnitudeContainer = container.querySelector(".magnitude");
        const magnitude = parseFloat(event.magnitude);
        magnitudeContainer.querySelector("span").textContent = magnitude;
        const category = this.getCategory(magnitude);
        magnitudeContainer.classList.add(category);
        container.querySelector("p[data-type=municipality]").textContent = event.municipality;
        container.querySelector("small[data-type=datetime]").innerHTML = this.getHoursAgo(event.datetime);
        container.querySelector("[data-type=depth]").textContent = `Depth: ${event.depth} km`;
        container.querySelector("[data-type=time]").innerHTML = `Happened ${this.getHoursAgo(event.datetime)}<small>${this.formatDate(event.datetime)}</small>`;
        container.querySelector("[data-type=location]").innerHTML = `${event.location}<small>${event.coords}</small>`;
        if(container.querySelector("a") !== null)
            container.querySelector("a").href = `/#/home/view/${event.id}`;
        container.setAttribute("data-id", event.id);
    }
    renderPrevious(events, limit = null) {
        if(limit === null)
            limit = events.length;
        const eventsContainer = document.querySelector(".events-container");
        const eventTemplate = document.querySelector("[data-template='event']");
        for(let i = 0; i < limit; i++) {
            const event = events[i];
            const eventContainer = eventTemplate.cloneNode(true);
            eventContainer.setAttribute("data-latitude", event.latitude);
            eventContainer.setAttribute("data-longitude", event.longitude);
            eventContainer.setAttribute("data-magnitude", event.magnitude);
            eventContainer.removeAttribute("data-template");
            eventsContainer.insertAdjacentElement("beforeend", eventContainer);
            this.renderEvent(eventContainer, event);
            this.addClickListener(eventContainer);
        }
    }
    removeActiveEvent() {
        document.querySelectorAll(".event").forEach(eventContainer => {
            eventContainer.classList.remove("active");
        });
        this.removePoints();
    }
    openEvent(container) {
        const latitude = parseFloat(container.getAttribute("data-latitude"));
        const longitude = parseFloat(container.getAttribute("data-longitude"));
        const magnitude = parseFloat(container.getAttribute("data-magnitude"));
        const coords = {
            lat: latitude,
            lng: longitude
        };
        this.removeActiveEvent();
        container.classList.add("active");
        this.map.panTo(coords);
        this.addPoint(this.map, magnitude, coords, true, true);
    }
    addClickListener(container) {
        container.addEventListener("click", e => {
            this.openEvent(container);
        });
    }
    getHoursAgo(inputDate) {
        const inputDateTime = new Date(inputDate);
        const currentDate = new Date();
        const timeDifference = Math.floor((currentDate - inputDateTime) / 1000); // Convert milliseconds to seconds

        const seconds = timeDifference % 60;
        const minutes = Math.floor(timeDifference / 60) % 60;
        const hours = Math.floor(timeDifference / 3600);
        const days = Math.floor(timeDifference / 86400);
        const weeks = Math.floor(timeDifference / 604800);
        const years = Math.floor(timeDifference / 31536000);

        let formattedTimeAgo = '';
        if (years > 0) {
            formattedTimeAgo = `${years} year${years !== 1 ? 's' : ''}`;
        } else if (weeks > 0) {
            formattedTimeAgo = `${weeks} week${weeks !== 1 ? 's' : ''}`;
        } else if (days > 0) {
            formattedTimeAgo = `${days} day${days !== 1 ? 's' : ''}`;
        } else if (hours > 0) {
            formattedTimeAgo = `${hours} hour${hours !== 1 ? 's' : ''}`;
        } else if (minutes > 0) {
            formattedTimeAgo = `${minutes} minute${minutes !== 1 ? 's' : ''}`;
        } else {
            formattedTimeAgo = `${seconds} second${seconds !== 1 ? 's' : ''}`;
        }
        return formattedTimeAgo + " ago";
    }
    formatDate(inputDate) {
        const inputDateTime = new Date(inputDate);
        const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
        const day = inputDateTime.getDate();
        const month = months[inputDateTime.getMonth()];
        const year = inputDateTime.getFullYear();
        const hours12 = inputDateTime.getHours() % 12 || 12;
        const minutesFormatted = inputDateTime.getMinutes().toString().padStart(2, '0');
        const amPm = inputDateTime.getHours() >= 12 ? 'PM' : 'AM';
    
        const formattedDate = `${month} ${day}, ${year} - ${hours12}:${minutesFormatted} ${amPm}`;
    
        return `${formattedDate}`;
    }
    getCategory(magnitude) {
        if(magnitude < 3) {
            return "category-1";
        } else if(magnitude >= 3 && magnitude < 4.5) {
            return "category-2";
        } else if(magnitude >= 4.5 && magnitude < 5.5) {
            return "category-3";
        } else if(magnitude >= 5.5 && magnitude < 6.5) {
            return "category-4";
        } else if(magnitude >= 6.5 && magnitude < 7.5) {
            return "category-5";
        } else if(magnitude >= 7.5) {
            return "category-6";
        }
    }
}